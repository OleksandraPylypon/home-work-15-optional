// 0! = 1
// n! = n * (n-1)!

// 2! = 1*2 = 2;
// 3! = 1*2*3 = 6;
// 4! = 1*2*3*4 = 24; 

let n = Number(prompt("Enter your number: "));
function factorial(n) {
  if (n === 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

console.log(factorial(n));